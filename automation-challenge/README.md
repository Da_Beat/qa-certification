# README.MD
 
## Automation Challenge
This proyect validate E2E an [e-commerce](http://automationpractice.com/index.php) page with Cypress framework for frontend testing.

## Proyect Structure
* Test files inside **integration** folder
* Reusable behavior functions inside **support** folder
* Data provider inside **fixtures** folder

## Installation
To install proyect dependencies just execute
```bash
npm install
```
To run test cases execute
```bash
npx cypress run
```
...or you can open the Cypress UI with
```bash
npx cypress open
```
## UI Automation Testing
If you want to learn more about this framework, you can visit [Cypress](https://www.cypress.io/)