// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (email, password) => {
  cy.get('.login').click()
  cy.get('#email').type(email)
  cy.get('#passwd').type(password)
  cy.get('#SubmitLogin > span').click()
})

Cypress.Commands.add('addItems', () => {
  cy.get('.homefeatured .ajax_add_to_cart_button > span')
  .should('have.length', 7)
  .each(($span, index, $spans) => {
    cy.wrap($span).click()
    cy.wait(2000)
    cy.get('.continue > span').click()
  })
})

Cypress.Commands.add('completeOrder', () => {
  cy.get('[title="View my shopping cart"]').click()
  cy.wait(2000)
  cy.get('#cart_title').should('include.text', 'Shopping-cart summary')
  cy.get('.cart_navigation > .button > span').click()
  cy.wait(2000)
  cy.get('.cart_navigation > .button > span').click()
  cy.wait(2000)
  cy.get('#cgv').check()
  cy.get('.cart_navigation > .button > span').click()
  cy.wait(2000)
  cy.get('.bankwire').click()
  cy.wait(2000)
  cy.get('#cart_navigation > .button > span').click()
  cy.wait(2000)
  cy.get('.page-heading').should('include.text', 'Order confirmation')
})