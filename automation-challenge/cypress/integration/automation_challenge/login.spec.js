/// <reference types="cypress" />
'use strict'

describe('Login feature testing', () => {
  beforeEach(() => {
    cy.fixture('newUser.json').as('userJson')
    cy.fixture('users.json').as('usersJson')
    cy.visit('/')
    cy.contains('h1', 'Automation Practice Website').should('be.visible')
  })

  it('1. Users can create an account with minimum information required', () => {
    cy.get('@userJson').then(user => {
      cy.get('.login').click()
      cy.get('#email_create').type(user.email)
      cy.get('#SubmitCreate > span').click()
      cy.get('#customer_firstname').type(user.firstname)
      cy.get('#customer_lastname').type(user.lastname)
      cy.get('#passwd').type(user.password)
      cy.get('#address1').type(user.address)
      cy.get('#city').type(user.city)
      cy.get('#id_state').select(user.state)
      cy.get('#postcode').type(user.zip)
      cy.get('#phone_mobile').type(user.phone)
      cy.get('#alias').clear().type(user.alias)
      cy.get('#submitAccount > span').click()
        cy.get('.account > span').should('have.text', `${user.firstname} ${user.lastname}`)
    })
  })

  it('2. Users can login using valid credentials', () => {
    cy.get('@usersJson').then(users => {
      cy.login(users[0].email, users[0].password)
        cy.get('.account > span').should('have.text', `${users[0].firstname} ${users[0].lastname}`)
    })
  })

  it('3.1 Users can not login using invalid email', () => {
    cy.get('@usersJson').then(users => {
      cy.login(users[1].email, users[1].password)
        cy.get('ol > li').should('include.text', 'Invalid email address')
    })
  })

  it('3.2 Users can not login using invalid password', () => {
    cy.get('@usersJson').then(users => {
      cy.login(users[2].email, users[2].password)
        cy.get('ol > li').should('include.text', 'Authentication failed')
    })
  })

  it('4. Users can logout from account\'s page', () => {
    cy.get('@usersJson').then(users => {
      cy.login(users[0].email, users[0].password)
        cy.get('.account > span').should('have.text', 'David Silva')
        cy.get('.logout').click()
        cy.wait(3000)
        cy.contains('.login', 'Sign in').should('be.visible')
    })
  })

  after(() => {
    cy.log('Login Tests Completed')
  })
})