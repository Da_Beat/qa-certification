/// <reference types="cypress" />
'use strict'

describe('Login feature testing', () => {
  beforeEach(() => {
    cy.fixture('users.json').as('usersJson')
    cy.visit('/')
    cy.contains('h1', 'Automation Practice Website').should('be.visible')
  })

  it('5. Users can add multiple items to the shopping cart', () => {
    cy.addItems()
  })

  it('6. Users can search for an existing product', () => {
    cy.get('#search_query_top').type('dress')
    cy.get('#searchbox > .btn').click()
    cy.wait(2000)
    cy.get('.heading-counter').invoke('text').then(text => {
      let n = text.trim()[0]
      cy.get('.product_list > li').should('have.length', Number(n))
    })
  })

  it('7. Users can complete a purchase', () => {
    cy.get('@usersJson').then(users => {
      cy.login(users[0].email, users[0].password)
      cy.get('.logo').click()
      cy.addItems()
      cy.completeOrder()
    })
  })

  after(() => {
    cy.log('Shopping Tests Completed')
  })
})