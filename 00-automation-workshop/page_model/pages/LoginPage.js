import { Selector, t } from 'testcafe'

class LoginPage {

  constructor() {
    this.usernameField = Selector('input[name="login.username"]')
    this.passwordField = Selector('input[name="login.password"]')
    this.loginButton = Selector('.btn.btn-primary').withExactText('Login')
    this.errorMessage = Selector('#login-error-message')
  }

  async submitLoginForm(username, password) {
    // await t.typeText(this.usernameField, username)
    // await t.typeText(this.passwordField, password)
    // await t.click(this.loginButton)

    await t
      .typeText(this.usernameField, username)
      .typeText(this.passwordField, password)
      .click(this.loginButton)
  }

}

export default new LoginPage()