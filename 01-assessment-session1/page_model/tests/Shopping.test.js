import { Selector, t } from 'testcafe'
import { CREDENTIALS } from '../data/Constants'
import LoginPage from '../pages/LoginPage'
import ProductsPage from '../pages/ProductsPage'
import ShoppingCartPage from '../pages/ShoppingCartPage'

fixture('Shopping feature testing')
  .page `https://www.saucedemo.com`
  .beforeEach(async t => {
    await LoginPage.submitLoginForm(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
  })

test('4. Users can navigate to the shopping cart', async t => {
  await ProductsPage.navigateShoppingCart()

    await t.expect(ShoppingCartPage.pageTittle.exists).ok()
    await t. expect(ShoppingCartPage.pageTittle.innerText).eql('Your Cart')
})

test('5. Users can add a single item to the shopping cart', async t => {
  const inventoryItemName = await ProductsPage.addSingleItem()
  await ProductsPage.navigateShoppingCart()

    await t.expect(ShoppingCartPage.cartItems.nth(0).exists).ok()
    await t.expect(ShoppingCartPage.cartItems.nth(0).find('.inventory_item_name').innerText).eql(inventoryItemName)
})

test('6. Users can add multiple items to the shopping cart', async t => {
  const [inventoryItemsCount, inventoryItemsNames] = await ProductsPage.addMultipleItems()
  await ProductsPage.navigateShoppingCart()
  const cartItemsCount = await ShoppingCartPage.cartItems.count

    await t.expect(cartItemsCount).eql(inventoryItemsCount)
    for(let i = 0; i < cartItemsCount; i++) {
      await t.expect(ShoppingCartPage.cartItems.nth(i).exists).ok()
      await t.expect(ShoppingCartPage.cartItems.nth(i).find('.inventory_item_name').innerText).eql(inventoryItemsNames[i])
    }

})