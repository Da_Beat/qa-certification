import { Selector, t } from 'testcafe'
import { CREDENTIALS } from '../data/Constants'
import LoginPage from '../pages/LoginPage'
import ProductsPage from '../pages/ProductsPage'
import ShoppingCartPage from '../pages/ShoppingCartPage'
import CheckoutPage from '../pages/CheckoutPage'
import OverviewPage from '../pages/OverviewPage'
import ConfirmationPage from '../pages/ConfirmationPage'


fixture('Checkout feature testing')
  .page `https://www.saucedemo.com`
  .beforeEach(async t => {
    await LoginPage.submitLoginForm(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
  })

test('7. Users can not continue with missing user information', async t => {
  await ProductsPage.addSingleItem()
  await ProductsPage.navigateShoppingCart()
  await ShoppingCartPage.checkout()
  
    await t.expect(CheckoutPage.pageTittle.exists).ok()
    await t.expect(CheckoutPage.pageTittle.innerText).eql('Checkout: Your Information')
  
  await CheckoutPage.continue()

    await t.expect(CheckoutPage.errorMessage.exists).ok()
    await t.expect(CheckoutPage.errorMessage.innerText).eql('Error: First Name is required')
})

test('8. Users can continue with all user information', async t => {
  await ProductsPage.addSingleItem()
  await ProductsPage.navigateShoppingCart()
  await ShoppingCartPage.checkout()
  await CheckoutPage.fillUserInformation()
  await CheckoutPage.continue()
  
    await t.expect(OverviewPage.pageTittle.exists).ok()
    await t.expect(OverviewPage.pageTittle.innerText).eql('Checkout: Overview')
})

test('9. Users can validate their added items', async t => {
  const [inventoryItemsCount, inventoryItemsNames] = await ProductsPage.addMultipleItems()
  await ProductsPage.navigateShoppingCart()
  await ShoppingCartPage.checkout()
  await CheckoutPage.fillUserInformation()
  await CheckoutPage.continue()
  const overviewItemsCount = await OverviewPage.overviewItems.count

    await t.expect(overviewItemsCount).eql(inventoryItemsCount)
    for(let i = 0; i < overviewItemsCount; i++) {
      await t.expect(OverviewPage.overviewItems.nth(i).exists).ok()
      await t.expect(OverviewPage.overviewItems.nth(i).find('.inventory_item_name').innerText).eql(inventoryItemsNames[i])
    }
})

test('10. Users can complete their order', async t => {
  await ProductsPage.addMultipleItems()
  await ProductsPage.navigateShoppingCart()
  await ShoppingCartPage.checkout()
  await CheckoutPage.fillUserInformation()
  await CheckoutPage.continue()
  await OverviewPage.finish()

    await t.expect(ConfirmationPage.pageTittle.exists).ok()
    await t.expect(ConfirmationPage.pageTittle.innerText).eql('Finish')
    await t.expect(ConfirmationPage.orderMessage.exists).ok()
    await t.expect(ConfirmationPage.orderMessage.innerText).eql('THANK YOU FOR YOUR ORDER')
})