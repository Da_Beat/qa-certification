import { Selector, t } from 'testcafe'

class OverviewPage {

  constructor() {
    this.pageTittle = Selector('.subheader')
    this.overviewItems = Selector('.cart_item')
    this.finishButton = Selector('.btn_action.cart_button')
  }

  async finish() {
    await t.click(this.finishButton)
  }

}

export default new OverviewPage()