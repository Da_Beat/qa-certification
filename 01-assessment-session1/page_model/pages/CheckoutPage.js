import { Selector, t } from 'testcafe'

class CheckoutPage {

  constructor() {
    this.pageTittle = Selector('.subheader')
    this.firtnameInput = Selector('#first-name')
    this.lastnameInput = Selector('#last-name')
    this.postalcodeInput = Selector('#postal-code')
    this.continueButton = Selector('.btn_primary.cart_button')
    this.errorMessage = Selector('h3[data-test="error"]')
  }

  async fillUserInformation() {
    await t
      .typeText(this.firtnameInput, 'David Martin')
      .typeText(this.lastnameInput, 'Silva Cruz')
      .typeText(this.postalcodeInput, '09360')
  }

  async continue() {
    await t.click(this.continueButton)
  }

}

export default new CheckoutPage()