import { Selector, t } from 'testcafe'

class ShoppingCartPage {

  constructor() {
    this.pageTittle = Selector('.subheader').withExactText('Your Cart')
    this.cartItems = Selector('.cart_item')
    this.checkoutButton = Selector('.btn_action.checkout_button')
  }

  async checkout() {
    await t.click(this.checkoutButton)
  }

}

export default new ShoppingCartPage()