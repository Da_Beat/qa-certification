import { Selector } from 'testcafe'

class ConfirmationPage {

  constructor() {
    this.pageTittle = Selector('.subheader')
    this.orderMessage = Selector('.complete-header')
  }

}

export default new ConfirmationPage()