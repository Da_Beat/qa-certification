import { Selector, t } from 'testcafe'

class ProductsPage {

  constructor() {
    this.pageTittle = Selector('.product_label')
    this.menuButton = Selector('#react-burger-menu-btn')
    this.logoutLink = Selector('#logout_sidebar_link')
    this.shoppingCartLink = Selector('.shopping_cart_link')
    this.inventoryItems = Selector('.inventory_item')
  }

  async logout() {
    await t
      .click(this.menuButton)
      .click(this.logoutLink)
  }

  async addSingleItem() {
    const inventoryItem = this.inventoryItems.nth(0)
    const inventoryItemName = await inventoryItem.find('.inventory_item_name').innerText
    await t.click(inventoryItem.find('.btn_primary.btn_inventory'))

    return  inventoryItemName
  }

  async addMultipleItems() {
    const inventoryItems = this.inventoryItems
    const inventoryItemsCount = await inventoryItems.count
    const inventoryItemsNames = []
    let inventoryItemsName
    for(let i = 0; i < inventoryItemsCount; i++) {
      inventoryItemsName = await inventoryItems.nth(i).find('.inventory_item_name').innerText
      inventoryItemsNames.push(inventoryItemsName)
      await t.click(inventoryItems.nth(i).find('.btn_primary.btn_inventory'))
    }

    return [inventoryItemsCount, inventoryItemsNames]
  }

  async navigateShoppingCart() {
    await t
      .click(this.shoppingCartLink)
  }
  
}

export default new ProductsPage()